(measure-command{
$i=0;
$ins = New-Object System.IO.StreamReader "E:\CTR\train.csv"
$outs = New-Object System.IO.StreamWriter "E:\CTR\train_modified.csv"
while( !$ins.EndOfStream) {
$line=$ins.ReadLine();
if( $i -ne 0 ) {
$outs.WriteLine($line);
}
$i=$i+1;
}
$outs.Close();
$ins.Close();
}
).TotalSeconds

(measure-command{
$i=0;
$ins = New-Object System.IO.StreamReader "E:\CTR\test.csv"
$outs = New-Object System.IO.StreamWriter "E:\CTR\test_modified.csv"
while( !$ins.EndOfStream) {
$line=$ins.ReadLine();
if( $i -ne 0 ) {
$outs.WriteLine($line);
}
$i=$i+1;
}
$outs.Close();
$ins.Close();
}
).TotalSeconds